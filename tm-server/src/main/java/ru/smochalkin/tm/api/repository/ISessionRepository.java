package ru.smochalkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull List<Session> findAllByUserId(@Nullable String userId);

}
