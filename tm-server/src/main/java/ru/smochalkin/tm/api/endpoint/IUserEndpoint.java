package ru.smochalkin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.Result;
import ru.smochalkin.tm.model.Session;
import ru.smochalkin.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    @SneakyThrows
    User findUserBySession(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @WebMethod
    @SneakyThrows
    Result userSetPassword(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "password") @Nullable String password
    );

    @WebMethod
    @SneakyThrows
    Result userUpdate(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName") @Nullable String middleName
    );

}
