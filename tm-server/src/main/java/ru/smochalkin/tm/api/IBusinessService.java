package ru.smochalkin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.model.AbstractBusinessEntity;

import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    @NotNull
    E create(@NotNull String userId, @Nullable String name, @Nullable String description);

    void clear(String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull String strSort);

    @NotNull
    List<E> findAll(@NotNull String userId);

    E findById(@NotNull String userId, @Nullable String id);

    @Nullable
    E findByName(@NotNull String userId, @Nullable String name);

    @NotNull
    E findByIndex(@NotNull String userId, @NotNull Integer index);

    void removeById(@NotNull String userId, @Nullable String id);

    void removeByName(@NotNull String userId, @Nullable String name);

    void removeByIndex(@NotNull String userId, @NotNull Integer index);

    void updateById(@Nullable String id, @Nullable String name, @Nullable String desc);

    void updateById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String desc);

    void updateByIndex(@NotNull String userId, @NotNull Integer index, @Nullable String name, @Nullable String desc);

    void updateStatusById(@NotNull String userId, @Nullable String id, @NotNull String status);

    void updateStatusByName(@NotNull String userId, @Nullable String name, @NotNull String status);

    void updateStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String status);

    boolean isNotIndex(@NotNull String userId, @Nullable Integer index);

}
