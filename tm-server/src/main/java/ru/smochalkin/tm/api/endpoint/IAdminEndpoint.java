package ru.smochalkin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.Result;
import ru.smochalkin.tm.model.Session;
import ru.smochalkin.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminEndpoint {

    @WebMethod
    Result createUser(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "email") @Nullable String email
    );

    @WebMethod
    Result removeUserById(@WebParam(name = "session") @NotNull Session session,
                        @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    Result removeUserByLogin(@WebParam(name = "session") @NotNull Session session,
                           @WebParam(name = "login") @Nullable String login
    );

    @WebMethod
    Result lockUserByLogin(@WebParam(name = "session") @NotNull Session session,
                         @WebParam(name = "login") @Nullable String login
    );

    @WebMethod
    Result unlockUserByLogin(@WebParam(name = "session") @NotNull Session session,
                           @WebParam(name = "login") @Nullable String login
    );

    @WebMethod
    List<User> findAllUsers(@WebParam(name = "session") @NotNull Session session);

    @WebMethod
    List<Session> findAllSessionsByUserId(@WebParam(name = "session") @NotNull Session session,
                                          @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    Result closeAllSessionsByUserId(@WebParam(name = "session") @NotNull Session session,
                                  @WebParam(name = "userId") @Nullable String userId
    );

}
