package ru.smochalkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<E> entities = findAll(userId);
        list.removeAll(entities);
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId) {
        return list.stream().filter(e -> userId.equals(e.getUserId())).collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return findAll(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    @NotNull
    public E findById(@NotNull final String userId, @Nullable final String id) {
        return findAll(userId).stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    public E findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(e -> name.equals(e.getName()))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    public E findByIndex(@NotNull final String userId, final int index) {
        return findAll(userId).get(index);
    }


    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final E entity = findById(userId, id);
        list.remove(entity);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final E entity = findByName(userId, name);
        list.remove(entity);
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final E entity = findByIndex(userId, index);
        list.remove(entity);
    }

    @Override
    public void updateById(@NotNull final String id, @NotNull final String name, @Nullable final String desc) {
        @NotNull final E entity = findById(id);
        entity.setName(name);
        entity.setDescription(desc);
    }

    @Override
    public void updateById(@NotNull final String userId,
                           @NotNull final String id,
                           @NotNull final String name,
                           @Nullable final String desc) {
        @NotNull final E entity = findById(userId, id);
        entity.setName(name);
        entity.setDescription(desc);
    }

    @Override
    public void updateByIndex(@NotNull final String userId,
                              @NotNull final Integer index,
                              @NotNull final String name,
                              @Nullable final String desc) {
        @NotNull final E entity = findByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(desc);
    }

    @Override
    public void updateStatusById(@NotNull String userId, @NotNull final String id, @NotNull final Status status) {
        @NotNull final E entity = findById(userId, id);
        entity.setStatus(status);
        updateDate(entity, status);
    }

    @Override
    public void updateStatusByName(@NotNull final String userId,
                                   @NotNull final String name,
                                   @NotNull final Status status) {
        @NotNull final E entity = findByName(userId, name);
        entity.setStatus(status);
        updateDate(entity, status);
    }

    @Override
    public void updateStatusByIndex(@NotNull final String userId, final int index, @NotNull final Status status) {
        @NotNull final E entity = findByIndex(userId, index);
        entity.setStatus(status);
        updateDate(entity, status);
    }

    @Override
    public int getCountByUser(@NotNull final String userId) {
        return (int) list.stream().filter(e -> userId.equals(e.getUserId())).count();
    }

    private void updateDate(final E entity, final Status status) {
        switch (status) {
            case IN_PROGRESS:
                entity.setStartDate(new Date());
                break;
            case COMPLETED:
                entity.setEndDate(new Date());
                break;
            case NOT_STARTED:
                entity.setStartDate(null);
                entity.setEndDate(null);
        }
    }

}
