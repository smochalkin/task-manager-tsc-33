package ru.smochalkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(@Nullable final E entity) {
        list.add(entity);
    }

    @Override
    public void addAll(@NotNull final List<E> entities) {
        list.addAll(entities);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    @NotNull
    public List<E> findAll() {
        return list;
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        return list.stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    @NotNull
    public E findById(@Nullable final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    public E remove(@NotNull final E entity) {
        list.remove(entity);
        return entity;
    }

    @Override
    public void removeAll(@NotNull final List<E> entities) {
        list.removeAll(entities);
    }

    @Override
    @NotNull
    public E removeById(@Nullable final String id) {
        @Nullable final E entity = findById(id);
        return remove(entity);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return list.stream().anyMatch(e -> e.getId().equals(id));
    }

}

