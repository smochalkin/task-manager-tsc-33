package ru.smochalkin.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.model.IHasName;

import java.util.Comparator;

public final class ComparatorByName implements Comparator<IHasName> {

    @NotNull
    public final static ComparatorByName INSTANCE = new ComparatorByName();

    @NotNull
    private ComparatorByName() {
    }

    @Override
    public int compare(@NotNull IHasName o1, @NotNull IHasName o2) {
        if (o1.getName() == null) return 1;
        if (o2.getName() == null) return -1;
        return o1.getName().compareTo(o2.getName());
    }

}