package ru.smochalkin.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.model.IWBS;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.util.DateAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    @Nullable
    protected String name;

    @Nullable
    protected String description;

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    @XmlJavaTypeAdapter(DateAdapter.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateAdapter.dateTransferPattern)
    protected Date created = new Date();

    @Nullable
    @XmlJavaTypeAdapter(DateAdapter.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateAdapter.dateTransferPattern)
    protected Date startDate;

    @Nullable
    @XmlJavaTypeAdapter(DateAdapter.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateAdapter.dateTransferPattern)
    protected Date endDate;

    @Nullable
    protected String userId;

    @Override
    @NotNull
    public String toString() {
        return id + ": " + name + ": " + description +
                "; Status - " + status.getDisplayName() +
                "; Created - " + created +
                "; Start - " + startDate +
                "; End - " + endDate +
                "; userId - " + userId;
    }

}
