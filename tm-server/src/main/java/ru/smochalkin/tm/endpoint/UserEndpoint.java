package ru.smochalkin.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.endpoint.IUserEndpoint;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.dto.Fail;
import ru.smochalkin.tm.dto.Result;
import ru.smochalkin.tm.dto.Success;
import ru.smochalkin.tm.model.Session;
import ru.smochalkin.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public User findUserBySession(
            @WebParam(name = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findById(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result userSetPassword(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "password") @Nullable final String password
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getUserService().setPassword(session.getUserId(), password);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result userUpdate(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getUserService().updateById(session.getUserId(), firstName, lastName, middleName);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
