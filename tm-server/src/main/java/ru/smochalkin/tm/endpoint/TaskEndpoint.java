package ru.smochalkin.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.endpoint.ITaskEndpoint;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.dto.Fail;
import ru.smochalkin.tm.dto.Result;
import ru.smochalkin.tm.dto.Success;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result createTask(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().create(session.getUserId(), name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeTaskStatusById(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().updateStatusById(session.getUserId(), id, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeTaskStatusByIndex(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().updateStatusByIndex(session.getUserId(), index, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeTaskStatusByName(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().updateStatusByName(session.getUserId(), name, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<Task> findTaskAllSorted(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "sort") @NotNull final String strSort
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId(), strSort);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<Task> findTaskAll(
            @WebParam(name = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Task findTaskById(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Task findTaskByName(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Task findTaskByIndex(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeTaskById(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().removeById(session.getUserId(), id);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeTaskByName(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().removeByName(session.getUserId(), name);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeTaskByIndex(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result clearTasks(
            @WebParam(name = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().clear(session.getUserId());
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateTaskById(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().updateById(session.getUserId(), id, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateTaskByIndex(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<Task> findTasksByProjectId(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "projectId") @NotNull final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findTasksByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result bindTaskByProjectId(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "projectId") @NotNull final String projectId,
            @WebParam(name = "taskId") @NotNull final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectTaskService().bindTaskByProjectId(session.getUserId(), projectId, taskId);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result unbindTaskByProjectId(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "projectId") @NotNull final String projectId,
            @WebParam(name = "taskId") @NotNull final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectTaskService().unbindTaskByProjectId(session.getUserId(), projectId, taskId);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
