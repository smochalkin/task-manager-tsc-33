package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.service.IProjectTaskService;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;

import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskByProjectId(@Nullable final String projectId, @Nullable final String taskId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        projectRepository.findById(projectId);
        taskRepository.findById(taskId);
        taskRepository.bindTaskById(projectId, taskId);
    }

    @Override
    public void bindTaskByProjectId(@NotNull final String userId,
                                    @Nullable final String projectId,
                                    @Nullable final String taskId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        projectRepository.findById(userId, projectId);
        taskRepository.findById(userId, taskId);
        taskRepository.bindTaskById(userId, projectId, taskId);
    }

    @Override
    public void unbindTaskByProjectId(@Nullable final String projectId, @Nullable final String taskId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        projectRepository.findById(projectId);
        taskRepository.findById(taskId);
        taskRepository.unbindTaskById(taskId);
    }

    @Override
    public void unbindTaskByProjectId(@NotNull final String userId,
                                      @Nullable final String projectId,
                                      @Nullable final String taskId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        projectRepository.findById(userId, projectId);
        taskRepository.findById(userId, taskId);
        taskRepository.unbindTaskById(userId, taskId);
    }

    @Override
    @NotNull
    public List<Task> findTasksByProjectId(@Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        return taskRepository.findTasksByProjectId(projectId);
    }

    @Override
    @NotNull
    public List<Task> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        return taskRepository.findTasksByProjectId(userId, projectId);
    }

    @Override
    public void removeProjectById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        taskRepository.removeTasksByProjectId(id);
        projectRepository.removeById(id);
    }

    @Override
    public void removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        Project project = projectRepository.findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        removeProjectById(project.getId());
    }

    @Override
    public void removeProjectByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (isEmpty(userId)) throw new EmptyIdException();
        Project project = projectRepository.findByIndex(userId, index);
        removeProjectById(project.getId());
    }

    @Override
    public boolean isNotProjectId(@Nullable final String id) {
        return projectRepository.findById(id) == null;
    }

    @Override
    public boolean isProjectName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        return projectRepository.findByName(userId, name) != null;
    }

    @Override
    public boolean isProjectIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) return false;
        return index < projectRepository.getCountByUser(userId);
    }

    @Override
    public void isTaskId(@NotNull final String id) {
        taskRepository.findById(id);
    }

}
