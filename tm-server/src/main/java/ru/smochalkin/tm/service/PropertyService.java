package ru.smochalkin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String PASSWORD_ITERATION = "iteration";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "99";

    @NotNull
    public static final String PASSWORD_SECRET = "secret";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    public static final String SIGN_ITERATION = "sign.iteration";

    @NotNull
    public static final String SIGN_ITERATION_DEFAULT = "88";

    @NotNull
    public static final String SIGN_SECRET = "sign.secret";

    @NotNull
    public static final String SIGN_SECRET_DEFAULT = "";

    @NotNull
    public static final String SERVER_HOST = "server.host";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String SERVER_PORT = "server.port";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "8800";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getValue(@NotNull final String name, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(name)) return System.getProperty(name);
        if (System.getenv().containsKey(name)) return System.getenv(name);
        return properties.getProperty(name, defaultValue);
    }

    @NotNull
    private Integer getValueInt(@NotNull final String name, @NotNull final String defaultValue) {
        @NotNull String value;
        if (System.getenv().containsKey(name)) {
            value = System.getenv(name);
        } else if (System.getProperties().containsKey(name)) {
            value = System.getenv(name);
        } else {
            value = properties.getProperty(name, defaultValue);
        }
        return Integer.valueOf(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read("version");
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return Manifests.read("email");
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return Manifests.read("developer");
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getValueInt(PASSWORD_ITERATION, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getSignSecret() {
        return getValue(SIGN_SECRET, SIGN_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSignIteration() {
        return getValueInt(SIGN_ITERATION, SIGN_ITERATION_DEFAULT);
    }

    @Override
    public @NotNull String getServerHost() {
        return getValue(SERVER_HOST, SERVER_HOST_DEFAULT);
    }

    @Override
    public @NotNull String getServerPort() {
        return getValue(SERVER_PORT, SERVER_PORT_DEFAULT);
    }

}

