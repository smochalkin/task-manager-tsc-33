package ru.smochalkin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.endpoint.*;
import ru.smochalkin.tm.api.repository.*;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.endpoint.*;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.repository.*;
import ru.smochalkin.tm.service.*;
import ru.smochalkin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;


@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    public final IUserRepository userRepository = new UserRepository();

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    public final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    public final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final Backup backup = new Backup(domainService);

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    private void init() {
        initPID();
        initData();
        initEndpoints();
        backup.init();
    }

    private void initEndpoints(){
        registryEndpoint(sessionEndpoint);
        registryEndpoint(adminEndpoint);
        registryEndpoint(userEndpoint);
        registryEndpoint(projectEndpoint);
        registryEndpoint(taskEndpoint);
    }

    private void registryEndpoint(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }


    private void initData() {
        @NotNull final String userId = userService.create("user", "1", "user@user.ru").getId();
        @NotNull final String adminId = userService.create("admin", "100", Role.ADMIN).getId();
        projectService.create(userId, "ProjectB", "lorem ipsum");
        projectService.create(userId, "ProjectD", "lorem ipsum").setStatus(Status.IN_PROGRESS);
        projectService.create(adminId, "ProjectA", "lorem ipsum").setStatus(Status.COMPLETED);
        projectService.create(adminId, "ProjectC", "lorem ipsum").setStatus(Status.IN_PROGRESS);
        taskService.create(userId, "TaskB", "lorem ipsum");
        taskService.create(userId, "TaskA", "lorem ipsum").setStatus(Status.COMPLETED);
        taskService.create(adminId, "TaskC", "lorem ipsum").setStatus(Status.IN_PROGRESS);
        taskService.create(adminId, "TaskD", "lorem ipsum").setStatus(Status.COMPLETED);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager-server.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO SERVER TASK MANAGER **");
        init();
    }

}
