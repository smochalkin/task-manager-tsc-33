package ru.smochalkin.tm.api.repository;

import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {
}
