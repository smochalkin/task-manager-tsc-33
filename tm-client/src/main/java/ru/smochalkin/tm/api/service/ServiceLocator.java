package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.endpoint.*;

public interface ServiceLocator extends IHasSession {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

}
