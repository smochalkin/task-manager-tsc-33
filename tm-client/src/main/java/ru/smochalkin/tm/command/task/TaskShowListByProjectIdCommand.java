package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.endpoint.Task;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowListByProjectIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-show-by-project-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Show tasks by project id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final List<Task> tasks = serviceLocator.getTaskEndpoint().findTasksByProjectId(serviceLocator.getSession(), projectId);
        for (Task task : tasks) {
            System.out.println(task.getId() + "|" + task.getName() + "|" + task.getStatus());
        }
    }

}
