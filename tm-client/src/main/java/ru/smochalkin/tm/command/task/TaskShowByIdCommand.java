package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.endpoint.Task;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-show-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Show task by id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(serviceLocator.getSession(), id);
        showTask(task);
    }

}
